package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {
        System.out.println(hasTeen(43,32,54));
    }
    public static boolean hasTeen(int num1, int num2, int num3) {
        return ((num1 >= 13 && num1 <= 19) || (num2 >= 13 && num2 <= 19) || (num3 >= 13 && num3 <= 19));
    }
    public static boolean isTeen(int num) {
        return num <= 19 && num >= 13;
    }
}
//        int[] teenArray = new int[]{13, 14, 15, 16, 17, 18, 19};
//        int[] inputArray = new int[]{num1, num2, num3};
//        for (int num : inputArray)
//            for (int age : teenArray)
//                return num == age;