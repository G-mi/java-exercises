package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {

        isCatPlaying(true, 10); // should return false since temperature is not in range 25 - 45
        isCatPlaying(false, 36); // should return false since temperature is not in range 25 - 35 (summer parameter is false)
        isCatPlaying(false, 35); // should return true since temperature is in range 25 - 35
    }
    public static boolean isCatPlaying(boolean summer, int temperature) {

        // The cats spend most of the day playing. In particular, they play if the temperature is
        // between 25 and 35 (inclusive). Unless it is summer,
        // then the upper limit is 45 (inclusive) instead of 35.

        if (!summer && temperature >= 25 && temperature <= 35) {
            return true;
        }
        else return summer && temperature >= 25 && temperature <= 45;
    }
}
