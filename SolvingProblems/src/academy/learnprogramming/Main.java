package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {

        printConversion(95.75);

    }
    public static long toMilesPerHour(double kilometersPerHour){

        double kilo = 1.609;

        if (kilometersPerHour < 0) {
            return -1;
        }
        else
            return Math.round(kilometersPerHour / kilo);

    }
    public static void printConversion(double kilometersPerHour){

        if (kilometersPerHour < 0){
            System.out.println("Invalid Value");
        }
        else System.out.println(kilometersPerHour + " km/h = " + toMilesPerHour(kilometersPerHour) + " mi/h");
    }
}
