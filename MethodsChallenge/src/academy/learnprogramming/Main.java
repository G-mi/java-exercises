package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {
        String name = "";
        int score = 1200;
        int highScorePosition = calculateHighScorePosition(score);

        highScorePosition = calculateHighScorePosition(1000);
        printThePosition("Jack", highScorePosition);

        highScorePosition = calculateHighScorePosition(700);
        printThePosition("Jenny", highScorePosition);

        highScorePosition = calculateHighScorePosition(300);
        printThePosition("Louis", highScorePosition);


    }
    public static int calculateHighScorePosition(int score) {

        if (score >= 1000) {
            return 1;
        } else if (score >= 500) {
            return 2;
        } if (score >= 100) {
            return 3;
        }
        return 4;

    }
    public static void printThePosition(String name, int highScorePosition) {
        System.out.println(name + " got to position: " + highScorePosition);
    }

}



