package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {
        // short
        // byte
        // long
        // int
        // float
        // double
        // char
        // boolean

        String myString = "This is a sting";
        System.out.println(myString);
        myString = myString + " this is more";
        System.out.println(myString);
        myString += " \u00A9";
        System.out.println(myString);
        String myLastString = "10";
        int myInt = 50;
        myLastString = myInt + myLastString;
        System.out.println(myLastString);
    }
}
