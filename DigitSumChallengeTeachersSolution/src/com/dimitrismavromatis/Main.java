package com.dimitrismavromatis;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println("The sum of the numbers 125 is " + sumDigits(125));
        System.out.println("The sum of the numbers -125 is " + sumDigits(-125));
        System.out.println("The sum of the numbers 4 is " + sumDigits(4));
        System.out.println("The sum of the numbers 32123 is " + sumDigits(32123));
    }


    public static int sumDigits(int number) {

        int sum = 0;

        if (number < 10) {
            return -1;
        }
        while (number > 0) {
            int digit = number % 10;
            sum += digit;
            number /= 10;
        }
        return sum;
    }
}
