package com.dimitrismavromatis;

import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.IntStream;

public class Main {

    private static String INVALID_VALUE_MESSAGE = "Invalid Value";

    public static void main(String[] args) {
	// write your code here

//        numberToWords(678346236);

        numberToWordsAlt(678346236);
    }

    public static void numberToWordsAlt(int number) {
        String s = String.format("%s", number);
        char[] chars = s.toCharArray();
        int charLength = chars.length;
        for (int i = 0; i < charLength; i++) {
            char c = chars[i];
            System.out.print(verbalise(c));
            if (i != charLength - 1) {
                System.out.print(" ");
            }
        }
    }

    // I don't like it. Try: using IntStream instead of CharArray. ALSO, time both above solution and the one below to see which is faster
    // WITH VERY LARGE NUMBERS

    private static String verbalise(char c) {
        switch (c) {
            case '-':
                return "Negative";
            case '0':
                return "Zero";
            case '1':
                return "One";
            case '2':
                return "Two";
            case '3':
                return "Three";
            case '4':
                return "Four";
            case '5':
                return "Five";
            case '6':
                return "Six";
            case '7':
                return "Seven";
            case '8':
                return "Eight";
            case '9':
                return "Nine";
        }
        return INVALID_VALUE_MESSAGE;
    }

    public static void numberToWords(int number) {
        if (number < 0) {
            System.out.println(INVALID_VALUE_MESSAGE);
        }

        int reverseNumber = 0;
        while (number > 0) {
            reverseNumber += (number % 10);
            number /= 10;
            if (number == 0) {
                break;
            }
                reverseNumber *= 10;

        }

        String stringNumber = "";
        while (reverseNumber> 0){
            if (reverseNumber % 10 == 0) {
                stringNumber += "Zero ";
            }
            else if (reverseNumber % 10 == 1) {
                stringNumber += "One ";
            }
            else if (reverseNumber % 10 == 2) {
                stringNumber += "Two ";
            }
            else if (reverseNumber % 10 == 2) {
                stringNumber += "Three ";
            }
            else if (reverseNumber % 10 == 4) {
                stringNumber += "Four ";
            }
            else if (reverseNumber % 10 == 5) {
                stringNumber += "Five ";
            }
            else if (reverseNumber % 10 == 6) {
                stringNumber += "Six ";
            }
            else if (reverseNumber % 10 == 7) {
                stringNumber += "Seven ";
            }
            else if (reverseNumber % 10 == 8) {
                stringNumber += "Eight ";
            }
            else if (reverseNumber % 10 == 9) {
                stringNumber += "Nine ";
            }
            else System.out.println(INVALID_VALUE_MESSAGE);
            reverseNumber/= 10;
        }
        System.out.println(stringNumber);
    }
}
