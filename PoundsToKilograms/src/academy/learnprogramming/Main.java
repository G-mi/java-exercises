package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {
        double key = 0.45359237d;
        double pounds = 200;
        double kilograms = pounds * key;
        System.out.println(pounds + " Pounds are equal to " + kilograms + " kilograms");
    }
}
