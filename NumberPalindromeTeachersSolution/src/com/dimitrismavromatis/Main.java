package com.dimitrismavromatis;

public class Main {

    public static void main(String[] args) {
	// write your code here

        int x = -333;

        System.out.println(isPalindrome(x));
    }

    public static boolean isPalindrome(int number) {

        if (number < 0) {
            number *= -1;
        }
        int reverse = 0;
        int cloneNumber = number;
        while (cloneNumber != 0) {
            reverse += cloneNumber % 10;
            cloneNumber /= 10;
            if (cloneNumber == 0) {
                break;
            }
            reverse *= 10;
        }

        return reverse == number;
    }
}
