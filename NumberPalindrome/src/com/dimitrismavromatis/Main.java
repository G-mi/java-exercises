package com.dimitrismavromatis;

import java.util.Objects;

public class Main {

    public static void main(String[] args) {
        // write your code here
//
//        String cloneNumber = String.valueOf(424);
//        System.out.println(cloneNumber);
        int testing = 23432;

        System.out.println("Is " + testing + " a palindrome?" + " The answer is " + isPalindrome(testing));
    }

    public static boolean isPalindrome(int number) {

        if (number < 0 ) {
            number *= -1;
        }
        int cloneNumber = number;
        StringBuilder palindrome = new StringBuilder();

        while (cloneNumber != 0) {
            palindrome.append(String.valueOf(cloneNumber % 10));
            cloneNumber -= (cloneNumber % 10);
            cloneNumber /= 10;
        }
        return Objects.equals(palindrome.toString(), String.valueOf(number));
    }
}
