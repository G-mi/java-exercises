package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {

        double value1 = 20.00d, value2 = 80.00d;
        double firstOp = (value1 + value2) * 100d;
        double modOp = firstOp % 40.00d;
        boolean myBool = (modOp == 0) ? true : false;
        System.out.println("My boolean is: " + myBool);
        if (!myBool) {
            System.out.println("Got some remainder");
        } else {
            System.out.println("Print this too");
        }
    }
}
