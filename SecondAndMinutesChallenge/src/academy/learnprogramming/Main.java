package academy.learnprogramming;

public class Main {

    private static final String INVALID_VALUE_MESSAGE = "Invalid value";

    public static void main(String[] args) {


        System.out.println(getDurationString(65, 45));
        System.out.println(getDurationString(3945L));
        System.out.println(getDurationString(-43));
        System.out.println(getDurationString(65,9));
    }

    public static String getDurationString(long minutes, long seconds) {

        if (minutes < 0 || seconds < 0) {
            return INVALID_VALUE_MESSAGE;
        }
        long hours = 0;
        String remainingSeconds = "";
        String remainingMinutes = "";
        String hoursToPrint = "";

        remainingSeconds += (seconds % 60);

        if (seconds >= 60) {
            minutes += (seconds / 60);
        }
        remainingMinutes += (minutes % 60);

        if (minutes >= 60) {
            hours += minutes / 60;
        }
        if (hours < 10) {
            hoursToPrint += hours;
            hoursToPrint = "0" + hoursToPrint;
        } else hoursToPrint += hours;

        if (minutes % 60 < 10) {
            remainingMinutes = "0" + remainingMinutes;
        }
        if (seconds % 60 < 10) {
            remainingSeconds = "0" + remainingSeconds;
        }

        return (hoursToPrint + "h " + remainingMinutes + "m " + remainingSeconds + "s ");
    }

    public static String getDurationString(long seconds) {
        if (seconds < 0) {
            return INVALID_VALUE_MESSAGE;
        }
        return getDurationString(0, seconds);
    }

}
