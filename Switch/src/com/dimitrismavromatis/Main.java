package com.dimitrismavromatis;

public class Main {

    public static void main(String[] args) {

        String month = "November";

        switch(month.toLowerCase()) {

            case "november":
                System.out.println("that is " + month);
                break;

            case "oKtober":
                System.out.println("OKt!");
                break;

            default:
                System.out.println("Didn't get that");
                break;
        }

//        String charValue = "G";
//
//        switch (charValue) {
//
//            case "A": case "B": case "C": case "D": case "E":
//                System.out.println(charValue + " was found");
//                break;
//
//            default:
//                System.out.println("Could not find A, B, C, D or E");
//                break;
//        }

//            int switchValue = 10;
//
//        switch(switchValue) {
//            case 1:
//                System.out.println("The value was 1");
//                break;
//
//            case 2:
//                System.out.println("The value was 2");
//                break;
//
//            case 3: case 4: case 5: case 6: case 7: case 8: case 9:
//                System.out.println("The value was 3, or 4, or 5, or 6, or 7, or 8, or 9");
//                System.out.println("It actually was " + switchValue);
//                break;
//
//            default:
//                System.out.println("Value was neither 1, or 2, or 3, or 4, or 5, or 6, or 7, or 8, or 9");
//                break;
//        }
    }
}
