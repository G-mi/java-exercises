package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {

        byte myByteNumber = 10;
        short myShortNumber = 20;
        int myIntNumber = 50;

        int mySum = myByteNumber + myShortNumber + myIntNumber;

        long myLongNumber = 50000L + (10L * mySum);

        System.out.println(myLongNumber);

        short shortTotal = (short) (1000 +
                10 * (mySum));
    }
}
