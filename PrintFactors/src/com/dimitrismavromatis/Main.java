package com.dimitrismavromatis;

public class Main {

    private static String INVALID_VALUE_MESSAGE = "Invalid Value";

    public static void main(String[] args) {
	// write your code here
        printFactors(-1);
    }

    /*
     * printFactors(6); → should print 1 2 3 6
     */

    public static void printFactors(int number) {
        if (number < 1) {
            System.out.println(INVALID_VALUE_MESSAGE);
        }

        String factors = "";

        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                factors += (i + " ");
            }
        }

        System.out.println(factors);
    }
}
