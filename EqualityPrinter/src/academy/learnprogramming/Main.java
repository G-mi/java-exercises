package academy.learnprogramming;

public class Main {

    private static String INVALID_VALUE_MESSAGE = "Invalid Value";

    public static void main(String[] args) {
	// write your code here
    }
    public static void printEqual(int x, int y, int z) {
        if ((x < 0) || (y < 0) || (z < 0)) {
            System.out.println(INVALID_VALUE_MESSAGE);
        }
        else if (x == y && y == z) {
            System.out.println("All numbers are equal");
        }
        else if (x != y && x != z && y != z) {
            System.out.println("All numbers are different");
        }
        else System.out.println("Neither all are equal or different");
    }
}
