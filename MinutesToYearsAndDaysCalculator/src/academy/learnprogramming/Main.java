package academy.learnprogramming;

public class Main {

    private static String INVALID_VALUE_MESSAGE = "Invalid Value";

    public static void main(String[] args) {

        printYearsAndDays(525600);
        printYearsAndDays(1051200);
        printYearsAndDays(561600);
	// write your code here
    }
    public static void printYearsAndDays(long minutes) {
        if (minutes < 0) {
            System.out.println(INVALID_VALUE_MESSAGE);
        } else {
            long days = (minutes / 60 / 24 % 365);
            long year = ((minutes / 60 / 24) / 365);
            System.out.println(minutes + " min = " + year + " y and " + days + " d");
        }
    }
}
