package com.dimitrismavromatis;

public class Main {

    public static void main(String[] args) {
	// write your code here
    }

    public static int getEvenDigitSum(int number) {

        if (number < 0) {
            return -1;
        }

        int sumOFevenNumbers = 0;
        while (number > 0) {

            if (number % 2 == 0) {
                sumOFevenNumbers += number % 10;
            }
            number /= 10;
        }
        return sumOFevenNumbers;
    }
}
