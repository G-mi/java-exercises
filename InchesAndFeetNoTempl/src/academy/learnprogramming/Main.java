package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {

        calcFeetAndInchesToCentimeters()
	// write your code here
    }

    public static double calcFeetAndInchesToCentimeters(double feet, double inches) {
        if (feet >= 0 && inches >= 0 && inches <= 12) {
            return -1;
        }
        else return ((inches / 12 + feet) * 2.54);

    }
}
