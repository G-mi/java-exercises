package com.dimitrismavromatis;

public class Main {

    public static void main(String[] args) {

//        for (int i=2; i<=8; i++) {
//            System.out.println("10,000 at "+ i +"% interest = " + String.format("%.2f", calculateInterest(10000.0, i)));
//        }
//
//        System.out.println("__________________\n");
//
//        for (int i=8; i>=2; i--) {
//            System.out.println("10,000 at "+ i +"% interest = " + String.format("%.2f", calculateInterest(10000.0, i)));
//        }

//        int count = 0;
//        for (int i = 20; i <= 30; i++) {
//            if (isPrime(i)) {
//                count ++;
//                System.out.println("Number "+ i +" is a prime number");
//                if (count == 3) {
//                    System.out.println("Exiting for loop");
//                    break;
//                }
//            }
//        }
        System.out.println(isPrime(12321));
    }

    public static double calculateInterest(double amount, double interestRate) {
        return amount * interestRate/100;
    }

//    Create a for statement using any range of numbers
//    Determine if prime using the method
//    if found print and increment the number of the prime numbers to be found
//    if that count is 3 exit the four loop
//    hint: use the break; statement to exit


    public static boolean isPrime(int n) {

        if(n == 1) {
            return false;
        }

        for(int i = 2; i <= (long) Math.sqrt(n); i++) {
            System.out.println("Looping " + i);
            if(n % i == 0) {
                return false;
            }
        }
        return true;
    }
}