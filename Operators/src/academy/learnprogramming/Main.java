package academy.learnprogramming;

public class Main {

    public static void main(String[] args) {

        boolean isCar = true;
        boolean wasCar = isCar ? true : false;
        if (wasCar) {
            System.out.println(wasCar);
        }
    }
}
