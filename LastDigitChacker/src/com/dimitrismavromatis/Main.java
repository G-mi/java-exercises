package com.dimitrismavromatis;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println(isValid(32));
        System.out.println(hasSameLastDigit(21, 24, 23));

    }

    public static boolean hasSameLastDigit(int num1, int num2, int num3) {
        if (num1 < 10 || num2 < 10 || num3 < 10 || num1 > 1000 || num2 > 1000 || num3 > 1000) {
            return false;
        }

        int n1Right = num1 % 10;
        int n2Right = num2 % 10;
        int n3Right = num3 % 10;

        return ((n1Right == n2Right) || (n1Right == n3Right) || (n2Right == n3Right));
    }

    public static boolean isValid(int number) {
        return ((number >= 10) && (number <= 1000));
    }
}

