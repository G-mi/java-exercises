package com.dimitrismavromatis;

public class Main {

    public static void main(String[] args) {
	// write your code here

        System.out.println(sumOdd(223, 243));
    }

    public static boolean isOdd(int number) {
        if (number < 0) {
            return false;
        }
        return number % 2 == 1;
    }

    public static int sumOdd(int start, int end){

        int sum = 0;

        if ((start < 0) || (end < 0) || (end < start)) {
            return -1;
        }
        else
            for (int i = start; i <= end; i++) {
                if (isOdd(i)) {
                    sum += i;
                    System.out.println(i);
                }
            }
            return sum;
    }
}
