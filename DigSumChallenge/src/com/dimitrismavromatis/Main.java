package com.dimitrismavromatis;

public class Main {

    public static void main(String[] args) {
	// write your code here

//        int feedingNumber = 125;
//        int feedingNumber = -125;
//        int feedingNumber = 4;
        int feedingNumber = 32123;

        System.out.println("The sum of the number "+ feedingNumber +" is " + (sumDigits(feedingNumber)));
    }

    public static int sumDigits(int number) {

        int modNumber = 10;
        int sum = 0;


        if (number < 10) {
            return -1;
        }
        do {
            sum += (number % modNumber);
            number -= (number % modNumber);
            number /= 10;

        } while (number != 0);

        return sum;
    }
}
