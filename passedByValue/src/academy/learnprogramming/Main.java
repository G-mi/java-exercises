//Coding with Liza!

package academy.learnprogramming;

import java.io.Console;

public class Main {

    public static void main(String[] args) {
	// write your code here
     /*   int num = 3;
        System.out.println("Num: " + num);
        var num2 = doSomething(num);
        System.out.println("Num2: " + num2);    // Should print 8
        System.out.println("Num: " + num);      // Should print 3 */

        var dog = new Dog();
        dog.Name = "Bill";
        System.out.println("Dog Name: " + dog.Name);
        DoSomethingWithADog(dog);
        System.out.println("Dog Name: " + dog.Name);
    }
//
//    private static int doSomething(int thing) {
//        thing = thing + 5;
//        return thing;
//    }

    private static void DoSomethingWithADog(Dog dog) {
        dog.Name = "Kill";
    }

    private static class Dog {
        public String Name;
    }
}