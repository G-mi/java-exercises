package com.dimitrismavromatis;

public class Main {

    public static void main(String[] args) {
	// write your code here
//
//        int count = 6;
//        while (count != 5) {
//            count ++;
//            System.out.println("Count value was "+count);
//        }
//        do {
//            System.out.println("Count value was "+count);
//            count ++;
//
//            if (count > 100) {
//                break;
//            }
//        }  while (count != 6);

        int number = 4;
        int finishNumber = 20;
        int evenNumbersFound = 0;

        while (number <= finishNumber) {
            number ++;
            if(!isEvenNumber(number)) {
                evenNumbersFound ++;
                continue;
            }
            System.out.println(number);
            if (evenNumbersFound == 5) {
                System.out.println(evenNumbersFound);
                break;
            }
        }
    }
    public static boolean isEvenNumber(int number) {
        return number % 2 == 0;
    }
}
