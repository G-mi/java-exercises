public class InchesAndFeet {

//    1 inch = 2.54cm and 1 foot = 12 inches

    public static double calcFeetAndInchesToCentimeters(double feet, double inches) {
        if ((feet < 0) || (inches < 0) || (inches > 12)) {
            System.out.println("Invalid inches or feet parameters");
            return -1;
        }
        double centimeters = (feet * 12 )* 2.54;
        centimeters += inches * 2.54;
        System.out.println(feet + " feet + " +inches+ " inches equals to: " +centimeters+ "cm");
        return centimeters;
    }
    public static double calcFeetAndInchesToCentimeters(double inches) {

        if (inches < 0) {
            return -1;
        }

        double feet = (int) inches / 12;
        double remainingInches = (int) inches % 12;
        System.out.println(inches + " inches are equal to " + feet + " feet and " + remainingInches + " inches");
        return calcFeetAndInchesToCentimeters(feet, remainingInches);
    }
}
